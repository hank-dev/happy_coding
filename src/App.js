import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import Modal from 'react-responsive-modal'
import './App.css'
import BoatRow from './BoatRow.js'
import axios from 'axios'
import $ from 'jquery'


class App extends Component {

    constructor(props) {

        super(props)
        
        this.state = {
            name: '',
            img: '',
            worker: ''
        }

        this.performSearch('boats') 

    }
 
    nameCreate = event => {

        this.setState({ name: event.target.value })
    }

    imgCreate = event => {

        this.setState({ img: event.target.value })
    }

    workerCreate = event => {

        this.setState({ worker: event.target.value})
    }
    
    createSubmit = () => {
        const name = this.state.name
        const img = this.state.img
        const worker = this.state.worker

        axios.post('http://localhost:3000/boats/', {name, img, worker}).then(res => {

            console.log(res)

            console.log(res.data)
        })
    }

//searh function
    performSearch(SearchBar) {

        console.log('Successful')
        
        const urlString = "http://localhost:3000/" + SearchBar
        
        $.ajax({
            
          url: urlString,
          success: (searchResults) => {
              
              const results = searchResults
              var boatRows = []

              results.forEach((boat) => {
                  
                  console.log(boat.name)
                  
                  const boatRow = <BoatRow key={boat.id} boat={boat}/>

                  boatRows.push(boatRow)
              })

              this.setState({rows: boatRows})
          },  
          error: (xhr, status, err) => {

              console.error('failed')
          }
        })
    }

      searchChangeHandler(event) {

          console.log(event.target.value)

          const bounceObject = this

          const SearchBar = 'boats?name=' + event.target.value

          bounceObject.performSearch(SearchBar)
      }


    state = {

        open: false,
  }

    onOpenModal = () => {

        this.setState({ open: true })
  }

    onCloseModal = () => {

        this.setState({ open: false })
  }

    render() {
    
        const { open } = this.state
    
        return (
          
          <div className='backGround'>
                
              <table className='titleBar'>

                  <tbody>
                        
                      <tr>
                          <td>

                              <img alt='app icon' width="100" src='cat-blue.png'/>

                          </td>

                          <td width='8'>

                          </td>

                          <td>

                          <h1>Awesome Boat Company</h1></td>

                        </tr>

                    </tbody>

                </table>

              <input className='SearchBar' onChange={this.searchChangeHandler.bind(this)} placeholder='Search'/>

              <div>
                  
                  <div className='option'>

                      <Button variant='contained' color='primary' onClick={this.onOpenModal}>Create</Button>

                  </div>  

                  <Modal
                      open={open}
                      onClose={this.onCloseModal}
                      center
                      classNames={{
                      transitionEnter: 'transition-enter',
                      transitionEnterActive: 'transition-enter-active',
                      transitionExit: 'transition-exit-active',
                      transitionExitActive: 'transition-exit-active',
                       }} 
                  >

                  <form onSubmit={this.createSubmit}>

                      <p></p>

                      <input type='text' name='name' className='form' onChange={this.nameCreate} placeholder='Boat Name'/>

                      <p></p>

                      <input type='text' name='img' className='form' onChange={this.imgCreate} placeholder='Put URL Here' />

                      <p></p>

                      <input type='text' name='worker' className='form' onChange={this.workerCreate} placeholder='Assigned Worker' />

                      <p></p>
                      
                      <Button variant='contained' color='primary' type='submit'>
                      
                          Submit
                      
                      </Button>

                  </form>

                  </Modal>

              </div>

                  {this.state.rows}

         </div>
        )
    }
}

export default App
  

